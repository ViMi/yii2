<?php
namespace frontend\models;


use yii\base\Model;
use yii\web\UploadedFile;

class UploadModel
        extends Model
{

    public $imageFile;
    public $imageCategory;



    public $oldImageName;
    public $imageObj;


    public function rules(){

        return [
          [['imageFile'], 'file',
              'skipOnEmpty'=>false,
              'extensions'=>['png', 'jpg'],
                'maxSize' => 1024*1024
              ],

        ];

    }

    public function upload(){



        $this->imageObj = new Image($this->imageCategory);


        $this->imageObj->setImageFile($this->imageFile);
        $str =  $this->imageObj->getStorageWay(). $this->imageObj->getNewName();


            if ($this->validate()) {
//Absolute way better (in YII2 may be Alias)
                $this->imageFile->saveAs("../../frontend/".$str);
                return true;

            }

        return false;


    }

}