<?php


namespace frontend\models;

use Yii;


class Image
{

    public $oldName;
    public $newName;
    public $categoryType;

    public $controlRules;
    public $imageFile;
    public $imageExtension;


    public $orientation = "landscape";

    const BASE_IMAGE_WAY = "web/images/";
    const LANDSCAPE = "landscape";
    const PORTRAIT = "portrait";



    function __construct(
        $typeOfImage = "product/",
        $rules = [
            "size"=>1024 * 1024,
            'xy_metric'=>[
                Image::LANDSCAPE=>['width'=>600,'height'=>800],
                Image::PORTRAIT=>['width'=>800, 'height'=>600]
            ]
        ],
        $orientation = Image::PORTRAIT){

        $this->controlRules = $rules;
        $this->categoryType = $typeOfImage;
        $this->orientation = $orientation;


    }



    public function validate(){

        list($width, $height, $type, $attr)
            = getimagesize("../".$this->getStorageWay().$this->newName);

        $size_xy=  "";




        if($this->orientation == Image::LANDSCAPE)
            $size_xy = $this->controlRules['xy_metric'][Image::LANDSCAPE];
        else if ($this->orientation == Image::PORTRAIT)
            $size_xy = $this->controlRules['xy_metric'][Image::LANDSCAPE];

        if($width > $size_xy['width'] && $height > $size_xy['height']) {
            if ($this->imageExtension == 'jpeg')
                imageresolution(imagecreatefromjpeg($this->imageFile),
                    $size_xy['width'], $size_xy['height']);
            if ($this->imageExtension == 'png')
                imageresolution(imagecreatefrompng($this->imageFile),
                    $size_xy['width'], $size_xy['height']);
                }

        return true;

    }


        public function getNewName() {

        $this->newName
            = Yii::$app->security->generateRandomString(32).".".$this->imageExtension;

            return $this->newName;
        }

        public function getOldName() {
            return $this->oldName;
        }


        public function getStorageWay() {

        if(empty($way))
            $way = Image::BASE_IMAGE_WAY.$this->categoryType;

            return $way;

        }


        public function getFolderWay(){


            return __DIR__."/../web/images/product/";

        }



        public function setImageFile($imageFile) {

                $this->oldName = $imageFile->baseName;
                $this->imageExtension = $imageFile->extension;
        }




}