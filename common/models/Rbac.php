<?php


namespace common\models\Rbac;


class Rbac
{

    ///CREATE RIGHT
    const CREATE_NEW_PRODUCT = 'createNewProduct';
    const CREATE_NEW_CATEGORY = 'createNewCategory';
    const CREATE_NEW_MANUFACTURER = 'createNewManufacturer';

////VIEW RIGHT
    const VIEW_ALL_PRODUCT = 'viewAllProduct';
    const VIEW_ALL_CATEGORY = 'viewAllCategory';
    const VIEW_ALL_MANUFACTURER = 'viewAllManufacturer';

////VIEW OWN RIGHT
    const VIEW_OWN_PRODUCT = "viewOwnProduct";
    const VIEW_OWN_CATEGORY = "viewOwnCategory";
    const VIEW_OWN_MANUFACTURER = "viewOwnManufacturer";


////UPDATE OWN RIGHT
    const UPDATE_OWN_PRODUCT = 'updateOwnProduct';
    const UPDATE_OWN_CATEGORY = 'updateOwnCategory';
    const UPDATE_OWN_MANUFACTURER = 'updateOwnManufacturer';

////UPDATE ALL RIGHT
    const UPDATE_ALL_PRODUCT = 'updateAllProduct';
    const UPDATE_ALL_CATEGORY = 'updateAllCategory';
    const UPDATE_ALL_MANUFACTURER = 'updateAllManufacturer';

///----INSERT RIGHT
    const INSERT_PRODUCT = 'insertProduct';
    const INSERT_CATEGORY = 'insertCategory';
    const INSERT_MANUFACTURER = 'insertManufacturer';

 ///-----DELETE ALL RIGHT
    const DELETE_ALL_PRODUCT = 'deleteAllProduct';
    const DELETE_ALL_MANUFACTURER = 'deleteAllManufacturer';
    const DELETE_ALL_CATEGORY = 'deleteAllCategory';


    //COMMON RIGHT (Union Xxx_OWN and Xxx_ALL STATEMENT)

    const DELETE_PRODUCT = "deleteProduct";
    const UPDATE_PRODUCT = "updateProduct";
    const VIEW_PRODUCT = "viewProduct";

    const DELETE_CATEGORY = "deleteCategory";
    const UPDATE_CATEGORY = "updateCategory";
    const VIEW_CATEGORY = "viewCategory";

    const DELETE_MANUFACTURER = "deleteManufacturer";
    const UPDATE_MANUFACTURER = "updateManufacturer";
    const VIEW_MANUFACTURER = "viewManufacturer";





///DESCRIPTION
///
///
///



    private $descriptions = [
        Rbac::CREATE_NEW_CATEGORY => "Can create new Category",
        Rbac::CREATE_NEW_MANUFACTURER => "Can create new Manufacturer",
        Rbac::CREATE_NEW_PRODUCT => "Can create new Product",
        Rbac::VIEW_PRODUCT => "View information about product",
        Rbac::VIEW_CATEGORY => 'View Information about category',
        Rbac::VIEW_MANUFACTURER => "View Information about manufacturer",
        Rbac::UPDATE_OWN_PRODUCT => "Update own record in Product table",
        Rbac::UPDATE_OWN_CATEGORY => "Update onw record in Category table",
        Rbac::UPDATE_OWN_MANUFACTURER => "Update own record in Manufacturer table",
        Rbac::INSERT_PRODUCT => "Insert new value in product table",
        Rbac::INSERT_CATEGORY => "Insert new value in category table",
        Rbac::INSERT_MANUFACTURER => "Insert new record in manufacturer table",
        Rbac::DELETE_ALL_PRODUCT => "Can delete all kind of records of products",
        Rbac::DELETE_ALL_MANUFACTURER => "Can delete all kind of records of manufacturer ",
        Rbac::DELETE_ALL_CATEGORY => "Can delete all kind of record on category",
        Rbac::VIEW_ALL_CATEGORY => "View Information about all categories",
        Rbac::VIEW_ALL_MANUFACTURER => "View Information about all manufacturer",
        Rbac::VIEW_ALL_PRODUCT => "View Information aobut all product",
        Rbac::VIEW_OWN_CATEGORY => "View Information about own created categories",
        Rbac::VIEW_OWN_MANUFACTURER => "View Information about own created manufacturer",
        Rbac::VIEW_OWN_PRODUCT => "View Information aobut own created product",
        Rbac::DELETE_CATEGORY => "Delete category record",
        Rbac::UPDATE_CATEGORY => "Update Catefory record",
        Rbac::DELETE_MANUFACTURER => "Delete manufacturer record",
        Rbac::DELETE_PRODUCT => "Delete Poduct record",
        Rbac::UPDATE_MANUFACTURER => "Update manufacturer record",
        Rbac::UPDATE_PRODUCT => "Update Product record",
        Rbac::UPDATE_ALL_CATEGORY => "Delete All Poduct record",
        Rbac::UPDATE_ALL_MANUFACTURER => "Update All manufacturer record",
        Rbac::UPDATE_ALL_PRODUCT => "Update All Product record"



    ];


    public static function getDescription($right) {
        $Rbac = new Rbac();

        return $Rbac->descriptions[$right];
    }


}