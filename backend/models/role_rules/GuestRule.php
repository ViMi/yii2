<?php
namespace backend\models\Rbac;

use yii\rbac\Rule;
use backend\models\Terms;

class GuestRule extends  Rule {

    public $name = "isGuest";

    public function execute($user, $item, $params)
    {
        return true;
    }


}