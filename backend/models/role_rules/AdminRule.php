<?php
namespace backend\models\Rbac;

use yii\rbac\Rule;
use backend\models\Terms;

class AdminRule extends  Rule {

    public $name = "isAdmin";

    public function execute($user, $item, $params)
    {
        return true;
    }


}