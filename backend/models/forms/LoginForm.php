<?php


namespace backend\models\forms;


use backend\models\User;
use Yii;
use yii\base\Model;

class LoginForm extends Model
{

    public $username;
    public $password;


    public function rules()
    {

        return [
            [['username', 'password'], 'required'],
            ['password', 'validatePassword'],
        ];


    }


    public function login()
    {
        if($this->validate()) {

            $user = User::findByUsername($this->username);

            return Yii::$app->user->login($user);
        }

        return false;
    }


    public function validatePassword($attribute, $params)
    {

        $user = User::findByUsername($this->username);

        if($user) {
            if(!$user->validatePassword($this->password))
            {
                $this->addError($attribute, "Incorrect password");

            }
        }

    }
}