<?php


namespace backend\models\Rbac;


class Roles
{

    const GUEST = "Guest";

    const AUTH_USER = "AuthUser";

    const MANAGER = "Manager";

    const ADMIN = "Admin";

    const SUPER_ADMIN = "SuperAdmin";




    private $descriptions = [
        Roles::GUEST => "GUEST ACCOUNT",
        Roles::AUTH_USER => "AUTHOR ACCOUNT",
        Roles::MANAGER => "MANAGER ACCOUNT",
        Roles::ADMIN => "ADMIN ACCOUNT" ,
        Roles::SUPER_ADMIN => "SUPER ADMIN ACCOUNT"
    ];


    public static function getDescription($roleName)
    {

        $role = new Roles();
        return $role->descriptions[$roleName];

    }


}